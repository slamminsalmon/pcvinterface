// global variables
int batteryVoltage = 0;
int batteryVoltagePin = A0;
int blinkAdjust = 0;
int blinkAdjustPin = A7;
// first visible ASCIIcharacter '!' is number 33:
int thisByte = 33;
int incomingByte = 0; // for incoming serial data
int byteCount = 0;
int digitalPin = 8;

float scaleVoltage(int voltageRead)
{
  return voltageRead * (5.0 / 1023.0);
}

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  // intitialize digital pin for bluetooth connectivity
  pinMode(digitalPin, INPUT);

  // setup serial comms for uart
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.print("Hello, welcome to the PCV Interface App\n");
}

void loop() {
  // read the batter voltage
  batteryVoltage = analogRead(batteryVoltagePin);
  // report to console
  Serial.print("Battery =\t");
  Serial.print(scaleVoltage(batteryVoltage));
  // read potentiometer adjustment
  blinkAdjust = analogRead(blinkAdjustPin);
  // report to console
  Serial.print("\tDelay = \t");
  Serial.println(blinkAdjust);
  
  // Adjust LED delays
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(blinkAdjust);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(blinkAdjust);  

  // is bluetooth connected to a client?
  if(digitalRead(digitalPin))
  {
    Serial.write("Bluetooth connected to client\n");
  }
  else
  {
    Serial.write("Bluetooth DISconnected from client\n");
  }
    // prints value unaltered, i.e. the raw binary version of the byte.
  // The Serial Monitor interprets all bytes as ASCII, so 33, the first number,
  // will show up as '!'
  Serial.print("ASCII:\t");
  
  Serial.write(thisByte);

  Serial.print("\tdec:\t");
  // prints value as string as an ASCII-encoded decimal (base 10).
  // Decimal is the default format for Serial.print() and Serial.println(),
  // so no modifier is needed:
  Serial.print(thisByte);
  // But you can declare the modifier for decimal if you want to.
  // this also works if you uncomment it:

  //Serial.print(thisByte, DEC);


  Serial.print("\thex:\t");
  // prints value as string in hexadecimal (base 16):
  Serial.print(thisByte, HEX);

  Serial.print("\toct:\t");
  // prints value as string in octal (base 8);
  Serial.print(thisByte, OCT);

  Serial.print("\tbin:\t");
  // prints value as string in binary (base 2) also prints ending line break:
  Serial.println(thisByte, BIN);

    // send data only when you receive data:
  while (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
    if (byteCount == 0)
    {
      // say what you got:
      Serial.print("I received: ");
      Serial.write(incomingByte);
    }
    else
    {
      Serial.write(incomingByte);
    }
    byteCount++;
  }
  byteCount = 0;
  // if printed last visible character '~' or 126, stop:
  if (thisByte == 126) {    // you could also use if (thisByte == '~') {
    // This loop loops forever and does nothing
    // TODO:  read from bluetooth here for exit program or print something
//    while (true) {
//      continue;
//    }
    thisByte = 33;
  }
  // go on to the next character
  thisByte++;

}
