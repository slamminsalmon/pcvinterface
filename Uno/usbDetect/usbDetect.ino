// global variables
int batteryVoltage = 0;
int batteryVoltagePin = A0;
int blinkAdjust = 0;
int blinkAdjustPin = A1;
// attempt to initialize a USB Host object
USBHost usb;

float scaleVoltage(int voltageRead)
{
  return voltageRead * (5.0 / 1023.0);
}

float scalePot(int potRead)
{
  return potRead / 4095;
}

void setup() {
  // put your setup code here, to run once:
    // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  // setup serial comms for uart
  Serial.begin(9600);
  Serial.print("Hello, welcome to the PCV Interface App\n");

  // potentiometer to speed up and slow down LED
  pinMode(blinkAdjustPin, INPUT);
  // voltage monitoring mocks bike battery voltage
  pinMode(batteryVoltagePin, INPUT);

  
}

void loop() {
  // read the batter voltage
  batteryVoltage = analogRead(batteryVoltagePin);
  // report to console
  Serial.print("Battery =\t");
  Serial.print(scaleVoltage(batteryVoltage));
  // read potentiometer adjustment
  blinkAdjust = analogRead(blinkAdjustPin);
  // report to console
  Serial.print("\tDelay = \t");
  Serial.println(blinkAdjust);
  
  // Adjust LED delays
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(blinkAdjust);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(blinkAdjust);  
}
